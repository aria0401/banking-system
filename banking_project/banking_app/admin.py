from django.contrib import admin
from .models import Customer, Rank, Account, Ledger

admin.site.register(Customer)
admin.site.register(Rank)
admin.site.register(Account)
admin.site.register(Ledger)

from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User


class Rank(models.Model):
    rank_name = models.CharField(max_length=10, blank=False)

    def __str__(self):
        return f'{self.rank_name}'

class Customer(models.Model):

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=250, unique=True)
    customer_rank = models.ForeignKey(Rank, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.first_name}'


class Account(models.Model):
    customer_id = models.ForeignKey(Customer, on_delete=CASCADE)

    def __str__(self):
        return  f'{self.customer_id}'

class Ledger(models.Model):
    account_id = models.ForeignKey(Account, on_delete=PROTECT)
    amount = models.DecimalField(max_length=10, decimal_place=2)
    transaction_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.amount} - {self.transaction_date}'

class Employee(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=250, unique=True)

    def __str__(self):
        return f'{self.first_name}'

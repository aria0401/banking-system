from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User


class Rank(models.Model):
    rank_name = models.CharField(max_length=10)

    def __str__(self):
        return f'{self.rank_name}'

class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=15)
    rank = models.ForeignKey(Rank, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.user.first_name} - {self.user.last_name} - {self.phone_number} - {self.rank}'

class Account(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    def __str__(self):
        return  f'{self.customer} - {self.pk}'


class Ledger(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    transaction_date = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return f'{self.account} - {self.amount}'
